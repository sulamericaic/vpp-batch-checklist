## vpp-batch-checklist


Batch responsável por processar todas as guias vo Vpp-Blaze-Checklist

* Processado de 20 em 20 minutos
* Todas as guias serão marcadas como processadas 
para não existir duplicidade de processamento

## Utilizado:

* Springbatch
* Springboot
* Java 8
* Cofre de Senhas
* Acessos as bases SaS Oracle e Postgree
* Projeto está na IC/OS

