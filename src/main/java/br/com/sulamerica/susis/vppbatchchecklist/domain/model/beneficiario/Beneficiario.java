package br.com.sulamerica.susis.vppbatchchecklist.domain.model.beneficiario;

import java.time.LocalDate;

public class Beneficiario {

    private LocalDate dataNascimento;

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
