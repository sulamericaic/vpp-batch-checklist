package br.com.sulamerica.susis.vppbatchchecklist.config.database.configuration;

import br.com.sulamerica.cofresenha.dto.Cofre;
import br.com.sulamerica.cofresenha.service.CofresenhaService;
import br.com.sulamerica.susis.vppbatchchecklist.config.database.properties.DataBasePropertiesCloud;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.util.logging.Logger;


@Configuration
@ConditionalOnProperty(name = "spring.datasource.provpp.cofreId")
public class DataBaseConfigurationCloud {


    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Bean(name = "provppdProperties")
    public HikariConfig hikariConfig(DataBasePropertiesCloud cloudProperties, CofresenhaService cofresenhaService) {
        Cofre cofre = cofresenhaService.buscarSegredoCofre(cloudProperties.getCofreId());
        if (cofre == null || StringUtils.isBlank(cofre.getSenha().getSenha())) {
            throw new IllegalArgumentException(
                    "Não foi possível obter a informação de autenticação para o DataSource CLOUD no Cofre-Senha. O cofre retornou vazio.");
        }

        logger.info("Configuração CLOUD carregada do Cofre-Senha.");
        logger.info("URL: " + cloudProperties.getUrl());
        logger.info("USU: " + cofre.getSenha().getUsername());

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setUsername(cofre.getSenha().getUsername());
        hikariConfig.setPassword(cofre.getSenha().getSenha());
        hikariConfig.setJdbcUrl(cloudProperties.getUrl());
        hikariConfig.setDriverClassName(cloudProperties.getClassName());
        hikariConfig.setConnectionTimeout(cloudProperties.getConnectionTimeout());
        hikariConfig.setMaxLifetime(cloudProperties.getMaxLifetime());
        hikariConfig.setConnectionTestQuery(cloudProperties.getConnectionTestQuery());
        hikariConfig.setMaximumPoolSize(cloudProperties.getMaxPoolSize());
        return hikariConfig;
    }

    @Bean(name = "provppdDataSource")
    public DataSource dataSource(@Qualifier("provppdProperties") HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Lazy
    @Bean(name = "provppJdbcTemplate")
    public JdbcTemplate jdbcTemplate(@Qualifier("provppdDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Lazy
    @Bean(name = "provppNamedJdbcTemplate")
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Qualifier("provppdDataSource") DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Lazy
    @Bean(name = "provppTransaction")
    public DataSourceTransactionManager dataSourceTransactionManager(
        @Qualifier("provppdDataSource")DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }
}