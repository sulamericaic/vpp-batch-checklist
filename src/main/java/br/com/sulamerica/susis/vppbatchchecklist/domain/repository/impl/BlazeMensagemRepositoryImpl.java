package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.impl;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.blaze.VppBlazeMensagem;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.BlazeMensagemRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;

@Repository
public class BlazeMensagemRepositoryImpl implements BlazeMensagemRepository {

    private NamedParameterJdbcTemplate provppNamedParameterJdbcTemplate;

    @Inject
    public BlazeMensagemRepositoryImpl(
            @Qualifier("provppNamedJdbcTemplate")NamedParameterJdbcTemplate provppNamedParameterJdbcTemplate) {
        this.provppNamedParameterJdbcTemplate = provppNamedParameterJdbcTemplate;
    }

    @Override
    public void incluiMensagem(Long numeroSolicitacaoVpp, VppBlazeMensagem vppBlazeMensagem, int indice) {
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO ow_provpp.SOLIC_VPP_BLAZE_MSG									");
        sql.append("            (NUM_SEQ_VPP_MSG, 												");
        sql.append("             NUM_SOLICITACAO_VPP, 											");
        sql.append("             COD_REGRA, 													");
        sql.append("             DSC_MSG, 														");
        sql.append("             DSC_CATEGORIA, 												");
        sql.append("             DSC_PROJETO, 													");
        sql.append("             TMP_ANALISE, 													");
        sql.append("             IDC_SINC_CLOUD)                                                ");
        sql.append("VALUES      ( 																");
        sql.append("			  NEXTVAL('OW_PROVPP.solic_vpp_blaze_msg_num_seq_vpp_msg_seq'),	");
        sql.append("              :numeroSolicitaca, 											");
        sql.append("              :codigoRegra, 												");
        sql.append("              :descricaoMensagem, 											");
        sql.append("              :descricaoCategoria, 											");
        sql.append("              :descricaoProjeto, 											");
        sql.append("              now(), 														");
        sql.append("              'S' )                                                         ");

        this.provppNamedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource()
                .addValue("numeroSolicitaca", numeroSolicitacaoVpp)
                .addValue("codigoRegra", vppBlazeMensagem.getCodigoRegra())
                .addValue("descricaoMensagem", vppBlazeMensagem.getDescricaoMensagem())
                .addValue("descricaoCategoria", vppBlazeMensagem.getDescricaoCategoria())
                .addValue("descricaoProjeto", vppBlazeMensagem.getDescricaoProjeto())
        );

    }
}