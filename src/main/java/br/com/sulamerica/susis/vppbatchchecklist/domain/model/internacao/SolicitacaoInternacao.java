package br.com.sulamerica.susis.vppbatchchecklist.domain.model.internacao;

public class SolicitacaoInternacao {

    private Integer idcCaraterSolicitacao;
    private Integer idcRegimeInternacao;
    private Integer idcTipoInternacao;

    public Integer getIdcCaraterSolicitacao() {
        return idcCaraterSolicitacao;
    }

    public void setIdcCaraterSolicitacao(Integer idcCaraterSolicitacao) {
        this.idcCaraterSolicitacao = idcCaraterSolicitacao;
    }

    public Integer getIdcRegimeInternacao() {
        return idcRegimeInternacao;
    }

    public void setIdcRegimeInternacao(Integer idcRegimeInternacao) {
        this.idcRegimeInternacao = idcRegimeInternacao;
    }

    public Integer getIdcTipoInternacao() {
        return idcTipoInternacao;
    }

    public void setIdcTipoInternacao(Integer idcTipoInternacao) {
        this.idcTipoInternacao = idcTipoInternacao;
    }
}
