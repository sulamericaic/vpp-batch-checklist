package br.com.sulamerica.susis.vppbatchchecklist.domain.service.impl;

import br.com.sulamerica.susis.vpp.blaze.client.ws.generate.*;
import br.com.sulamerica.susis.vppbatchchecklist.domain.blaze.util.XMLGregorianCalendarUtil;
import br.com.sulamerica.susis.vppbatchchecklist.domain.enums.TipoSolicitacao;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.conselhoprofissional.ConselhoProfissional;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.internacao.SolicitacaoInternacao;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.CodigoCaraterAtendimento;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Servico;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Solicitacao;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.BeneficiarioRepository;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.CrmRepository;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.DetalheInternacaoRepository;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.ServicoRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.Exception;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class SolicitacaoApServiceImpl {

    @Autowired
    private ServicoRepository servicoRepository;

    @Autowired
    private CrmRepository crmRepository;

    @Autowired
    private BeneficiarioRepository beneficiarioRepository;

    @Autowired
    private DetalheInternacaoRepository detalheInternacaoRepository;

    public SolicitacaoAp constroiPayloadChecklist(Solicitacao solicitacao){
        SolicitacaoAp solicitacaoAp = new SolicitacaoAp();

        solicitacaoAp.setBeneficiario(buildBeneficiario(solicitacao));
        solicitacaoAp.setCodigoEmpresa(buildEmpresa(solicitacao));
        solicitacaoAp.setIdcTipoSolicitacao(buildTipoSolicitacao(solicitacao));
        solicitacaoAp.setTipo(solicitacaoAp.getIdcTipoSolicitacao());
        solicitacaoAp.getSolicitacoesApItems().addAll(buildSolicitacaoApItem(solicitacao.getNumeroSolicitacao()));
        solicitacaoAp.setPrestador(buildPrestador(solicitacao.getCodigoPrestador()));

        if(TipoSolicitacao.INTERNACAO.getIdcTipoSolicitacao().equals(solicitacaoAp.getIdcTipoSolicitacao())){
            solicitacaoAp.setInternacao(buildInternacao(solicitacao));
        }

        br.com.sulamerica.susis.vpp.blaze.client.ws.generate.Crm crmBlaze = new br.com.sulamerica.susis.vpp.blaze.client.ws.generate.Crm();
        ConselhoProfissional conselho = constroiConselhoProfissional(solicitacao.getNumeroSolicitacao());

        crmBlaze.setNumero(conselho.getNumero());
        crmBlaze.setUf(conselho.getSigla());
        solicitacaoAp.setCodigoConselho(conselho.getCodigo());
        solicitacaoAp.setCrm(crmBlaze);
        return solicitacaoAp;
    }


    /**
     * Construir objeto Beneficiario do Blaze a partir da Solicitacao obtida na query inicial
     * @param solicitacao
     * @return
     */
    private Beneficiario buildBeneficiario(Solicitacao solicitacao){
        Beneficiario beneficiarioBlaze = new Beneficiario();
        Optional<br.com.sulamerica.susis.vppbatchchecklist.domain.model.beneficiario.Beneficiario> beneficiario = beneficiarioRepository.buscarDataNascimentoBeneficiario(
                solicitacao.getCodempresa(),
                solicitacao.getCodPrefEmpresa(),
                solicitacao.getCodFamiliarBenef(),
                solicitacao.getCodRdp());

        if(beneficiario.isPresent()){
            beneficiarioBlaze.setDataNascimento(
                    XMLGregorianCalendarUtil.convertDate(beneficiario.get().getDataNascimento()));
        }
        return beneficiarioBlaze;
    }

    /**
     * Controi node com o codigo do prestador
     *
     * @param codigoPrestador
     * @return
     */
        private Prestador buildPrestador(String codigoPrestador){
            Prestador prestadorBlaze = new Prestador();
            prestadorBlaze.setCodigo(codigoPrestador);
            return prestadorBlaze;
        }

    /**
     * Contruir node de internacao. Deve retorna carater da solicitacao (Eletivo/Urgente) e regime da internacao
     * (domiciliar e etc)
     *
     * @param solicitacao
     * @return
     */
    private SolicitacaoApInternacao buildInternacao(Solicitacao solicitacao){
        SolicitacaoApInternacao internacaoBlaze = new SolicitacaoApInternacao();
        Optional<SolicitacaoInternacao> detalheInternacao = detalheInternacaoRepository.buscarDadosInternacao(solicitacao.getNumeroSolicitacao());
        if(detalheInternacao.isPresent()){
            internacaoBlaze.setIdcCaraterSolicitacao(
                    CodigoCaraterAtendimento.byId(detalheInternacao.get().getIdcCaraterSolicitacao()).getSigla());
            internacaoBlaze.setIdcRegimeInternacao(detalheInternacao.get().getIdcRegimeInternacao());
            Integer idcTipoInternacao = detalheInternacao.get().getIdcTipoInternacao();
            internacaoBlaze.setIdcTipoInternacao(idcTipoInternacao);
            return internacaoBlaze;
        }
        return internacaoBlaze;
    }


    /**
     * Construir chave empresa (codPrefEmpresa + codEmpresa) para validacao nos anexos do Checklist
     * @param solicitacao
     * @return
     */
    private String buildEmpresa(Solicitacao solicitacao){
        return solicitacao.getCodPrefEmpresa() +
                solicitacao.getCodempresa();
    }

    /**
     * Avalia o codigo do idcTipoSolicitacao e converte para parametro String esperado pelo checklist
     * @param solicitacao
     * @return
     */
    private String buildTipoSolicitacao(Solicitacao solicitacao){
        String idcTipoSolicitacao = null;

        switch(solicitacao.getIdcTipoSolicitacao()){
            case 0:
                return TipoSolicitacao.SP_SADT.getIdcTipoSolicitacao();
            case 1:
                idcTipoSolicitacao = TipoSolicitacao.INTERNACAO.getIdcTipoSolicitacao();
                break;
            case 2:
                idcTipoSolicitacao = TipoSolicitacao.PRORROGACAO.getIdcTipoSolicitacao();
                break;
            case 3:
                idcTipoSolicitacao = TipoSolicitacao.OPME.getIdcTipoSolicitacao();
                break;
            case 4:
                idcTipoSolicitacao = TipoSolicitacao.RADIOTERAPIA.getIdcTipoSolicitacao();
                break;
            case 5:
                idcTipoSolicitacao = TipoSolicitacao.QUIMIOTERAPIA.getIdcTipoSolicitacao();
                break;
            default:
                break;
        }
        return idcTipoSolicitacao;
    }

    /**
     * Obter lista de servicos independente do tipo de solicitacao
     * @param numeroSolicitacao
     * @return
     */
    private List<SolicitacaoApItem> buildSolicitacaoApItem(Long numeroSolicitacao){
        List<SolicitacaoApItem> solicitacoesApItensBlaze = Lists.newArrayList();
        List<Servico> servicos = servicoRepository.buscarServicos(numeroSolicitacao);
        servicos.forEach(servico -> {
            SolicitacaoApItem solicitacaoApItem = new SolicitacaoApItem();
            //31032019 - Ajuste para montar node de servicos no XML do Checklist
            solicitacaoApItem.setServico(buildServico(servico.getCodigoServico()));
            solicitacoesApItensBlaze.add(solicitacaoApItem);
        });
        return solicitacoesApItensBlaze;
    }

    private br.com.sulamerica.susis.vpp.blaze.client.ws.generate.Servico buildServico(Integer codigoServico){
        br.com.sulamerica.susis.vpp.blaze.client.ws.generate.Servico servicoBlaze = new br.com.sulamerica.susis.vpp.blaze.client.ws.generate.Servico();
        servicoBlaze.setCodigo(codigoServico);
        return servicoBlaze;
    }


    private ConselhoProfissional constroiConselhoProfissional(Long numeroSolicitacao){
        ConselhoProfissional conselho = new ConselhoProfissional();

        try{
            Map<String, Object> dadosConselho = crmRepository.findDadosConselho(numeroSolicitacao);
            String codigoConselho = (String) dadosConselho.get("SIG_CONS_PROFESSIONAL");
            BigDecimal numeroConselhoSolic = (BigDecimal) dadosConselho.get("NUM_CONS_PROF_SOLIC");
            String siglaUF = (String) dadosConselho.get("SIG_UF");
            conselho.setNumero(numeroConselhoSolic.longValue());
            conselho.setSigla(siglaUF);
            conselho.setCodigo(codigoConselho);
        }catch (Exception e ){
            return new ConselhoProfissional();
        }
        return conselho;
    }


}
