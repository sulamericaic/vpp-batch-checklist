package br.com.sulamerica.susis.vppbatchchecklist.domain.blaze.builder;

import br.com.sulamerica.susis.vpp.blaze.client.ws.generate.ItemCheckList;
import br.com.sulamerica.susis.vpp.blaze.client.ws.generate.RetornoSolicitacaoApBlaze;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.blaze.VppBlazeMensagem;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.isNull;

public class VppBlazeMensagemBuilder {

    public static VppBlazeMensagemBuilder builder(){
        return new VppBlazeMensagemBuilder();
    }

    public List<VppBlazeMensagem> build(List<ItemCheckList> regrasChecklist){
        List<VppBlazeMensagem> mensagens = new ArrayList<>();
        if(!CollectionUtils.isEmpty(regrasChecklist)){
            regrasChecklist.stream()
                    //.filter(Objects::isNull)
                    .forEach(m ->{
                        mensagens.add(this.parse(m));
                    });
        }
        return mensagens;
    }

    private VppBlazeMensagem parse(ItemCheckList m){
        VppBlazeMensagem vppBlazeMensagem = new VppBlazeMensagem();
        vppBlazeMensagem.setCodigoRegra(m.getNomeRegra());
        vppBlazeMensagem.setDescricaoMensagem(m.getMensagemCheckList());
        vppBlazeMensagem.setDescricaoCategoria(m.getResumo());
        vppBlazeMensagem.setDescricaoProjeto(m.getProjeto());
        return vppBlazeMensagem;
    }

    public List<VppBlazeMensagem> build(RetornoSolicitacaoApBlaze retorno) {
        if(isNull(retorno) || isNull(retorno.getRegrasCheckLists())) {
            return Collections.emptyList();
        }
        return this.build(retorno.getRegrasCheckLists());
    }
}
