package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.impl;

public class SolicitacaoRepositoryImpl{

    private SolicitacaoRepositoryImpl() {
    }

    public static String buscarSolicitacoes() {
        return new StringBuilder()
                .append("  select  vpp.num_solicitacao_vpp,                                                          ")
                .append("          vpp.dat_inclusao,                                                                 ")
                .append("          vpp.idc_tipo_solicitacao,                                                         ")
                .append("          vpp.cod_prestador,                                                                ")
                .append("          vpp.cod_empresa,                                                                  ")
                .append("          vpp.cod_pref_empresa,                                                             ")
                .append("          vpp.cod_familiar_benef,                                                           ")
                .append("          vpp.cod_rdp                                                                       ")
                .append("    from  OW_PROVPP.solicitacao_vpp vpp                                                     ")
                .append("   where                                                                                    ")
                .append("     not exists (select msg.num_solicitacao_vpp from ow_provpp.solic_vpp_blaze_msg msg      ")
                .append("                  where msg.num_solicitacao_vpp = vpp.num_solicitacao_vpp)                  ")
                .append("  and vpp.dat_inclusao between TO_TIMESTAMP('13072019 0000', 'DDMMYYYY HH24MI')             ")
                .append("                           and TO_TIMESTAMP('31122099 2359', 'DDMMYYYY HH24MI')             ")
                .append("  limit 800                                                                                 ")
        .toString();
    }
}
