package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Solicitacao;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SolicitacaoMapper implements RowMapper<Solicitacao> {
    @Override
    public Solicitacao mapRow(ResultSet rs, int rowNum) throws SQLException {
        Solicitacao solicitacao = new Solicitacao();
        solicitacao.setNumeroSolicitacao(rs.getLong("NUM_SOLICITACAO_VPP"));
        solicitacao.setDatInclusaoSolicitacao(rs.getDate("DAT_INCLUSAO"));
        solicitacao.setIdcTipoSolicitacao(rs.getInt("IDC_TIPO_SOLICITACAO"));
        solicitacao.setCodigoPrestador(rs.getString("COD_PRESTADOR"));
        solicitacao.setCodempresa(rs.getString("COD_EMPRESA"));
        solicitacao.setCodPrefEmpresa(rs.getString("COD_PREF_EMPRESA"));
        solicitacao.setCodFamiliarBenef(rs.getString("COD_FAMILIAR_BENEF"));
        solicitacao.setCodRdp(rs.getString("COD_RDP"));
        return solicitacao;
    }
}
