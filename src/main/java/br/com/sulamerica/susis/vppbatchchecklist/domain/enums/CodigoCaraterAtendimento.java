package br.com.sulamerica.susis.vppbatchchecklist.domain.enums;

public enum CodigoCaraterAtendimento {

    ELETIVO(1, "E"), URGENCIA_EMERGENCIA(2 , "U");

    private Integer codigo;
    private String sigla;

    public Integer getCodigo() {
        return codigo;
    }

    public String getSigla() {
        return sigla;
    }


    private CodigoCaraterAtendimento(Integer codigo, String sigla) {
        this.codigo = codigo;
        this.sigla = sigla;
    }

    public static CodigoCaraterAtendimento byId(Integer id) {
        CodigoCaraterAtendimento codigoCaraterAtendimento = null;

        for (CodigoCaraterAtendimento c : CodigoCaraterAtendimento.values()) {
            if (c.getCodigo().equals(id)) {
                codigoCaraterAtendimento = c;
                break ;
            }
        }

        return codigoCaraterAtendimento;
    }
}
