package br.com.sulamerica.susis.vppbatchchecklist.domain.enums;

public enum OrigemExecucao {

    BATCH("BATCH"),
    MANUAL("MANUAL");

    private String codigo;

    OrigemExecucao(String codigo){
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
