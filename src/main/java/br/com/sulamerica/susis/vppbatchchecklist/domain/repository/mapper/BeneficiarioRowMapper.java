package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.beneficiario.Beneficiario;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static br.com.sulamerica.susis.vppbatchchecklist.domain.blaze.util.DateUtil.toLocalDate;

public class BeneficiarioRowMapper implements RowMapper<Beneficiario> {
    @Override
    public Beneficiario mapRow(ResultSet rs, int rowNum) throws SQLException {
        Beneficiario beneficiario = new Beneficiario();
        beneficiario.setDataNascimento(toLocalDate(rs.getDate("DAT_NASCIMENTO")));
        return beneficiario;
    }
}
