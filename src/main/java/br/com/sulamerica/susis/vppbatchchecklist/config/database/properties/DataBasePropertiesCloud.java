package br.com.sulamerica.susis.vppbatchchecklist.config.database.properties;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@ConditionalOnProperty(name = "spring.datasource.provpp.cofreId")
@Component
@Validated
@ConfigurationProperties(prefix = "spring.datasource.provpp")
public class DataBasePropertiesCloud {

    private Integer cofreId;

    @NotNull
    private String url;

    @NotNull
    private String className;

    private String connectionTestQuery = "";
    private Long connectionTimeout = 120000L;
    private Long maxLifetime = 600000L;
    private Integer maxPoolSize = 10;
    private String packageName;

    public Integer getCofreId() {
        return cofreId;
    }

    public void setCofreId(Integer cofreId) {
        this.cofreId = cofreId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getConnectionTestQuery() {
        return connectionTestQuery;
    }

    public void setConnectionTestQuery(String connectionTestQuery) {
        this.connectionTestQuery = connectionTestQuery;
    }

    public Long getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Long connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Long getMaxLifetime() {
        return maxLifetime;
    }

    public void setMaxLifetime(Long maxLifetime) {
        this.maxLifetime = maxLifetime;
    }

    public Integer getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(Integer maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
