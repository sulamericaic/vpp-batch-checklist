package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.internacao.SolicitacaoInternacao;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class DetalheInternacaoRowMapper implements RowMapper<SolicitacaoInternacao> {
    @Override
    public SolicitacaoInternacao mapRow(ResultSet resultSet, int i) throws SQLException {
        SolicitacaoInternacao internacao = new SolicitacaoInternacao();
        internacao.setIdcCaraterSolicitacao(resultSet.getInt("cod_carater_atend_ans"));
        internacao.setIdcRegimeInternacao(resultSet.getInt("cod_reg_internacao"));
        internacao.setIdcTipoInternacao(resultSet.getInt("cod_tipo_intern"));
        return internacao;
    }
}
