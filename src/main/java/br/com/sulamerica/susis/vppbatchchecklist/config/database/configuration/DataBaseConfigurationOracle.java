package br.com.sulamerica.susis.vppbatchchecklist.config.database.configuration;

import br.com.sulamerica.cofresenha.dto.Cofre;
import br.com.sulamerica.cofresenha.service.CofresenhaService;
import br.com.sulamerica.susis.vppbatchchecklist.config.database.properties.DataBasePropertiesOracle;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.util.logging.Logger;

@Configuration
@ConditionalOnProperty(name = "spring.datasource.oracle.cofreId")
public class DataBaseConfigurationOracle {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Bean
    @Primary
    public HikariConfig hikariConfig(DataBasePropertiesOracle oracleProperties, CofresenhaService cofresenhaService) {
        Cofre cofre = cofresenhaService.buscarSegredoCofre(oracleProperties.getCofreId());
        if (cofre == null || StringUtils.isBlank(cofre.getSenha().getSenha())) {
            throw new IllegalArgumentException(
                    "Não foi possível obter a informação de autenticação para o DataSource ORACLE no Cofre-Senha. O cofre retornou vazio.");
        }

        logger.info("Configuração ORACLE carregada do Cofre-Senha.");
        logger.info("URL: " + oracleProperties.getUrl());
        logger.info("USU: " + cofre.getSenha().getUsername());

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setUsername(cofre.getSenha().getUsername());
        hikariConfig.setPassword(cofre.getSenha().getSenha());
        hikariConfig.setJdbcUrl(oracleProperties.getUrl());
        hikariConfig.setDriverClassName(oracleProperties.getClassName());
        hikariConfig.setConnectionTimeout(oracleProperties.getConnectionTimeout());
        hikariConfig.setMaxLifetime(oracleProperties.getMaxLifetime());
        hikariConfig.setConnectionTestQuery(oracleProperties.getConnectionTestQuery());
        hikariConfig.setMaximumPoolSize(oracleProperties.getMaxPoolSize());
        return hikariConfig;
    }

    @Bean
    @Primary
    public DataSource dataSource(HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    @Primary
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    @Primary
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean(name = "oracleTransaction")
    @Primary
    public DataSourceTransactionManager dataSourceTransactionManager(DataSource datasource) {
        return new DataSourceTransactionManager(datasource);
    }
}
