package br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao;

public class Crm {

    private Long numero;
    private String uf;
    private String situacao;
    private Long numeroConselho;

    public Long getNumeroConselho() {
        return numeroConselho;
    }

    public void setNumeroConselho(Long numeroConselho) {
        this.numeroConselho = numeroConselho;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
}
