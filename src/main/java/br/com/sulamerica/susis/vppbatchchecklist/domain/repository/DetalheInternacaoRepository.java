package br.com.sulamerica.susis.vppbatchchecklist.domain.repository;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.internacao.SolicitacaoInternacao;

import java.util.Optional;

public interface DetalheInternacaoRepository {

    Optional<SolicitacaoInternacao> buscarDadosInternacao(Long numSolicitacao);

}
