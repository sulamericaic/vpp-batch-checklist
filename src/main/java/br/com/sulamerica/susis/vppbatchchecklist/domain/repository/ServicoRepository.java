package br.com.sulamerica.susis.vppbatchchecklist.domain.repository;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Servico;

import java.util.List;

public interface ServicoRepository {

    List<Servico> buscarServicos(Long numeroSolicitacao);

}
