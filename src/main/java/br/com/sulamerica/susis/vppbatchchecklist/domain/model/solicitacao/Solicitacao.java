package br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.blaze.VppBlazeMensagem;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.conselhoprofissional.ConselhoProfissional;

import java.util.Date;
import java.util.List;

public class Solicitacao {


    private Long numeroSolicitacao;

    private Date datInclusaoSolicitacao;

    private int idcTipoSolicitacao;

    private String codigoPrestador;

    private String codempresa;

    private String codPrefEmpresa;

    private String codFamiliarBenef;

    private String codRdp;

    private ConselhoProfissional conselhoProfissional;

    private Servico servico;

    public Long getNumeroSolicitacao() {
        return numeroSolicitacao;
    }

    public void setNumeroSolicitacao(Long numeroSolicitacao) {
        this.numeroSolicitacao = numeroSolicitacao;
    }

    public Date getDatInclusaoSolicitacao() {
        return datInclusaoSolicitacao;
    }

    public void setDatInclusaoSolicitacao(Date datInclusaoSolicitacao) {
        this.datInclusaoSolicitacao = datInclusaoSolicitacao;
    }

    public int getIdcTipoSolicitacao() {
        return idcTipoSolicitacao;
    }

    public void setIdcTipoSolicitacao(int idcTipoSolicitacao) {
        this.idcTipoSolicitacao = idcTipoSolicitacao;
    }

    public String getCodigoPrestador() {
        return codigoPrestador;
    }

    public void setCodigoPrestador(String codigoPrestador) {
        this.codigoPrestador = codigoPrestador;
    }

    public String getCodempresa() {
        return codempresa;
    }

    public void setCodempresa(String codempresa) {
        this.codempresa = codempresa;
    }

    public String getCodPrefEmpresa() {
        return codPrefEmpresa;
    }

    public void setCodPrefEmpresa(String codPrefEmpresa) {
        this.codPrefEmpresa = codPrefEmpresa;
    }

    public String getCodFamiliarBenef() {
        return codFamiliarBenef;
    }

    public void setCodFamiliarBenef(String codFamiliarBenef) {
        this.codFamiliarBenef = codFamiliarBenef;
    }

    public String getCodRdp() {
        return codRdp;
    }

    public void setCodRdp(String codRdp) {
        this.codRdp = codRdp;
    }

    public ConselhoProfissional getConselhoProfissional() {
        return conselhoProfissional;
    }

    public void setConselhoProfissional(ConselhoProfissional conselhoProfissional) {
        this.conselhoProfissional = conselhoProfissional;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }
}
