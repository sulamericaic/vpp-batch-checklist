package br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao;

public enum CodigoCaraterAtendimento {
    ELETIVO(1, "E"),
    URGENCIA_EMERGENCIA(2, "U"),
    ELETIVO_ANS(1, "E"),
    URGENCIA_EMERGENCIA_ANS(2, "U");

    private Integer codigo;
    private String sigla;

    CodigoCaraterAtendimento(Integer codigo, String sigla) {
        this.codigo = codigo;
        this.sigla = sigla;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
    public static CodigoCaraterAtendimento byId(Integer id) {
        CodigoCaraterAtendimento codigoCaraterAtendimento = null;
        for (CodigoCaraterAtendimento c : CodigoCaraterAtendimento.values()) {
            if (c.getCodigo().equals(id)) {
                codigoCaraterAtendimento = c;
                break ;
            }
        }
        return codigoCaraterAtendimento;
    }
}
