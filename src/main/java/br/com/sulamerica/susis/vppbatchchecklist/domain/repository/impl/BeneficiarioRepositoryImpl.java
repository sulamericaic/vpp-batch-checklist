package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.impl;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.beneficiario.Beneficiario;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.BeneficiarioRepository;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper.BeneficiarioRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.Optional;

@Repository
public class BeneficiarioRepositoryImpl implements BeneficiarioRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Inject
    public BeneficiarioRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Optional<Beneficiario> buscarDataNascimentoBeneficiario(String codigoEmpresa, String codigoPrefixoEmpresa, String codigoFamiliarBeneficiario, String codigoRdp) {
        StringBuilder sql = new StringBuilder();
        sql.append("    SELECT BEN.DAT_NASCIMENTO                             ");
        sql.append("      FROM SDE_BENEFICIARIO BEN                           ");
        sql.append("     WHERE BEN.COD_EMPRESA        = :codEmpresa           ");
        sql.append("       AND BEN.COD_PREF_EMPRESA   = :codPrefEmpresa       ");
        sql.append("       AND BEN.COD_FAMILIAR_BENEF = :codFamiliarBenef     ");
        sql.append("       AND BEN.COD_RDP            = :codRdp               ");

        return this.namedParameterJdbcTemplate.query(sql.toString(), new MapSqlParameterSource()
                        .addValue("codEmpresa", codigoEmpresa)
                        .addValue("codPrefEmpresa", codigoPrefixoEmpresa)
                        .addValue("codFamiliarBenef", codigoFamiliarBeneficiario)
                        .addValue("codRdp", codigoRdp),
                new BeneficiarioRowMapper()).stream().findFirst();
    }
}
