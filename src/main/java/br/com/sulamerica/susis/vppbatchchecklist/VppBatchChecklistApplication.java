package br.com.sulamerica.susis.vppbatchchecklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({
		"br.com.sulamerica.susis",
		"br.com.sulamerica.susis.vpp.blaze.client",
		"br.com.sulamerica.susis.vpp.blaze"
})
public class VppBatchChecklistApplication {

	public static void main(String[] args) {
		SpringApplication.run(VppBatchChecklistApplication.class, args).close();
	}

}
