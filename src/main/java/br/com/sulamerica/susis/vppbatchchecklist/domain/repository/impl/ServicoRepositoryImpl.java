package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.impl;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Servico;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.ServicoRepository;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper.ServicoMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

@Repository
public class ServicoRepositoryImpl implements ServicoRepository {

    private NamedParameterJdbcTemplate provppNamedParameterJdbcTemplate;

    @Inject
    public ServicoRepositoryImpl(@Qualifier("provppNamedJdbcTemplate")NamedParameterJdbcTemplate provppNamedParameterJdbcTemplate) {
        this.provppNamedParameterJdbcTemplate = provppNamedParameterJdbcTemplate;
    }

    /**
     * Retorna todos os servicos independente do tipo de solicitacao
     * @param numeroSolicitacao
     * @return codServico
     */
    @Override
    public List<Servico> buscarServicos(Long numeroSolicitacao) {
        StringBuilder sql = new StringBuilder();
        sql.append("  select coalesce (internacao.cod_servico, sadt.cod_servico, prorrog.cod_servico, radio.cod_servico) as cod_servico         ");
        sql.append("    from ow_provpp.solicitacao_vpp vpp                                                                                      ");
        sql.append("    full join ow_provpp.solic_sp_sadt_item        sadt        on sadt.num_solicitacao_vpp         = vpp.num_solicitacao_vpp ");
        sql.append("    full join ow_provpp.solic_internacao_item     internacao  on internacao.num_solicitacao_vpp   = vpp.num_solicitacao_vpp ");
        sql.append("    full join ow_provpp.solic_intern_pror_item    prorrog     on prorrog.num_solicitacao_vpp      = vpp.num_solicitacao_vpp ");
        sql.append("    full join ow_provpp.solic_radioterapia_item   radio       on radio.num_solicitacao_vpp        = vpp.num_solicitacao_vpp ");
        sql.append("  where vpp.num_solicitacao_vpp = :numSolicitacaoVpp                                                                        ");

        return provppNamedParameterJdbcTemplate.query(sql.toString(), new MapSqlParameterSource("numSolicitacaoVpp", numeroSolicitacao),
                new ServicoMapper());
    }
}
