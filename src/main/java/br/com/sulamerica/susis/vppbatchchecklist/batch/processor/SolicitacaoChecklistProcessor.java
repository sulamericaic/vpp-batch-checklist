package br.com.sulamerica.susis.vppbatchchecklist.batch.processor;

import br.com.sulamerica.susis.vpp.blaze.client.component.CheckListClient;
import br.com.sulamerica.susis.vpp.blaze.client.ws.generate.InvokeCheckListResponse;
import br.com.sulamerica.susis.vpp.blaze.client.ws.generate.SolicitacaoAp;
import br.com.sulamerica.susis.vppbatchchecklist.domain.blaze.builder.VppBlazeMensagemBuilder;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Solicitacao;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.blaze.VppBlazeMensagem;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.BlazeMensagemRepository;
import br.com.sulamerica.susis.vppbatchchecklist.domain.service.impl.SolicitacaoApServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SolicitacaoChecklistProcessor implements ItemProcessor<Solicitacao, Solicitacao> {

    public static final Logger log = LoggerFactory.getLogger(SolicitacaoChecklistProcessor.class);

    @Autowired
    private CheckListClient checkListClient;

    @Autowired
    private SolicitacaoApServiceImpl solicitacaoApService;

    @Autowired
    private BlazeMensagemRepository blazeMensagemRepository;

    /**
     * Metodo de processamento da guia no Blaze Checklist
     * @param solicitacao
     * @return Lista VppBlazeMensagem
     * @throws Exception
     */

    @Override
    public Solicitacao process(final Solicitacao solicitacao) throws Exception {
        SolicitacaoAp solicitacaoAp = solicitacaoApService.constroiPayloadChecklist(solicitacao);
        InvokeCheckListResponse checkListResponse = checkListClient.obterDados(solicitacaoAp);
        List<VppBlazeMensagem> mensagens = VppBlazeMensagemBuilder.builder().build(checkListResponse.getReturn());
        for (VppBlazeMensagem mensagem : mensagens){
            blazeMensagemRepository.incluiMensagem(solicitacao.getNumeroSolicitacao(), mensagem, 0);
        }
        return solicitacao;
    }
}