package br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao;

public class Servico {

    private Integer codigoServico;

    public Integer getCodigoServico() {
        return codigoServico;
    }

    public void setCodigoServico(Integer codigoServico) {
        this.codigoServico = codigoServico;
    }
}