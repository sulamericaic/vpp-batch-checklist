package br.com.sulamerica.susis.vppbatchchecklist.domain.blaze.util;


import java.sql.Date;
import java.time.LocalDate;

import static java.util.Objects.isNull;

public class DateUtil {

    private DateUtil(){}

    public static LocalDate toLocalDate(Date date) {
        if(isNull(date)) {
            return null;
        }
        return date.toLocalDate();
    }


}
