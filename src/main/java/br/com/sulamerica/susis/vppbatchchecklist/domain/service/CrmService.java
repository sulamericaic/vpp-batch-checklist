package br.com.sulamerica.susis.vppbatchchecklist.domain.service;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.conselhoprofissional.ConselhoProfissional;

public interface CrmService {

    ConselhoProfissional busca(Long numeroSolicitacao);

    ConselhoProfissional buscaPorSolicitacao(Long numeroSolicitacao);

}
