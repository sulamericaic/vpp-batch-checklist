package br.com.sulamerica.susis.vppbatchchecklist.domain.blaze.util;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.GregorianCalendar;

import static java.util.Objects.isNull;

public class XMLGregorianCalendarUtil {

    private XMLGregorianCalendarUtil(){}

    private static DatatypeFactory datatypeFactory;

    static {
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static XMLGregorianCalendar convertDate(LocalDate localDate) {
        if(isNull(localDate)) {
            return null;
        }

        GregorianCalendar gc = GregorianCalendar.from(localDate.atStartOfDay(ZoneId.of("America/Sao_Paulo")));
        XMLGregorianCalendar xmlGregorialCalendar = datatypeFactory.newXMLGregorianCalendar(gc);
        return xmlGregorialCalendar;
    }
}
