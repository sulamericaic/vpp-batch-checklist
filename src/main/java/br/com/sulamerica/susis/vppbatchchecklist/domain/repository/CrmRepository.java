package br.com.sulamerica.susis.vppbatchchecklist.domain.repository;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.conselhoprofissional.ConselhoProfissional;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Crm;

import java.util.Map;
import java.util.Optional;

public interface CrmRepository {

    Optional<Crm> buscarConselhoProfissional(Long numeroSolicitacao);

    Optional<Crm> buscarConselhoCompleto(Long numeroConselho, Long codigoUfAns);

    Optional<ConselhoProfissional> buscaSiglaConselhoProfissional(Long numConselhoProfissional, Long codigoAns);

    Map<String, Object> findDadosConselho(Long numeroSolicitacao);

}
