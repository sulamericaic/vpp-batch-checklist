package br.com.sulamerica.susis.vppbatchchecklist.domain.enums;

public enum TipoSolicitacao {
    SP_SADT(0, "A"),
    INTERNACAO(1, "I"),
    PRORROGACAO(2, "P"),
    OPME(3, "O"),
    RADIOTERAPIA(4, "R"),
    QUIMIOTERAPIA(5, "Q")
    ;

    private int idTipoSolicitacao;
    private String idcTipoSolicitacao;

    TipoSolicitacao(int idTipoSolicitacao, String idcTipoSolicitacao) {
        this.idTipoSolicitacao = idTipoSolicitacao;
        this.idcTipoSolicitacao = idcTipoSolicitacao;
    }

    public int getIdTipoSolicitacao() {
        return idTipoSolicitacao;
    }

    public void setIdTipoSolicitacao(int idTipoSolicitacao) {
        this.idTipoSolicitacao = idTipoSolicitacao;
    }

    public String getIdcTipoSolicitacao() {
        return idcTipoSolicitacao;
    }

    public void setIdcTipoSolicitacao(String idcTipoSolicitacao) {
        this.idcTipoSolicitacao = idcTipoSolicitacao;
    }
}
