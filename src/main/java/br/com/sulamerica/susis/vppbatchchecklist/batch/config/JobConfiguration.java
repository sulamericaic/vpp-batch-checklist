package br.com.sulamerica.susis.vppbatchchecklist.batch.config;

import br.com.sulamerica.susis.vppbatchchecklist.batch.processor.SolicitacaoChecklistProcessor;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Solicitacao;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.impl.SolicitacaoRepositoryImpl;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper.SolicitacaoMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.inject.Inject;
import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class JobConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JdbcTemplate provppNamedParameterJdbcTemplate;

    @Autowired
    @Qualifier("provppdDataSource")
    private DataSource postgreDataSource;

    @Inject
    public JobConfiguration(@Qualifier("provppJdbcTemplate")JdbcTemplate provppNamedParameterJdbcTemplate) {
        this.provppNamedParameterJdbcTemplate = provppNamedParameterJdbcTemplate;
    }

    @Bean
    BatchConfigurer configurer(@Qualifier("dataSource") DataSource dataSource){
        return new DefaultBatchConfigurer(dataSource);
    }

    @Bean
    //@Scheduled(fixedDelay = 500)
    public Job processarSolicitacaoJob (JobBuilderFactory jobBuilderFactory,
                                        StepBuilderFactory stepBuilderFactory,
                                        ItemReader<Solicitacao> solicitacaoItemReader,
                                        ItemProcessor<Solicitacao, Solicitacao> solicitacaoItemProcessor){
        Step step1 = stepBuilderFactory.get("processarSolicitacaoChecklist")
                .<Solicitacao, Solicitacao> chunk(800)
                .reader(solicitacaoItemReader)
                .processor(solicitacaoItemProcessor)
                .build();
        return jobBuilderFactory.get("processarSolicitacaoChecklist")
                .incrementer(new RunIdIncrementer())
                .start(step1)
                .build();
    }

    @Bean
    ItemReader<Solicitacao> solicitacaoItemReader(){
        JdbcCursorItemReader<Solicitacao> dataBaseReader = new JdbcCursorItemReader<>();
        dataBaseReader.setDataSource(postgreDataSource);
        dataBaseReader.setSql(SolicitacaoRepositoryImpl.buscarSolicitacoes());
        dataBaseReader.setRowMapper(new SolicitacaoMapper());
        return dataBaseReader;
    }

    @Bean
    SolicitacaoChecklistProcessor solicitacaoItemProcessor(){
        return new SolicitacaoChecklistProcessor();
    }

}