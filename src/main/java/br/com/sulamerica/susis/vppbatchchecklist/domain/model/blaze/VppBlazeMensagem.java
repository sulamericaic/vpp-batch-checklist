package br.com.sulamerica.susis.vppbatchchecklist.domain.model.blaze;

public class VppBlazeMensagem {

    private Long numeroSeqVppMensagem;
    private Long numeroSolicitacao;
    private String codigoRegra;
    private String descricaoMensagem;
    private String descricaoCategoria;
    private String descricaoProjeto;

    public Long getNumeroSeqVppMensagem() {
        return numeroSeqVppMensagem;
    }

    public void setNumeroSeqVppMensagem(Long numeroSeqVppMensagem) {
        this.numeroSeqVppMensagem = numeroSeqVppMensagem;
    }

    public Long getNumeroSolicitacao() {
        return numeroSolicitacao;
    }

    public void setNumeroSolicitacao(Long numeroSolicitacao) {
        this.numeroSolicitacao = numeroSolicitacao;
    }

    public String getCodigoRegra() {
        return codigoRegra;
    }

    public void setCodigoRegra(String codigoRegra) {
        this.codigoRegra = codigoRegra;
    }

    public String getDescricaoMensagem() {
        return descricaoMensagem;
    }

    public void setDescricaoMensagem(String descricaoMensagem) {
        this.descricaoMensagem = descricaoMensagem;
    }

    public String getDescricaoCategoria() {
        return descricaoCategoria;
    }

    public void setDescricaoCategoria(String descricaoCategoria) {
        this.descricaoCategoria = descricaoCategoria;
    }

    public String getDescricaoProjeto() {
        return descricaoProjeto;
    }

    public void setDescricaoProjeto(String descricaoProjeto) {
        this.descricaoProjeto = descricaoProjeto;
    }
}
