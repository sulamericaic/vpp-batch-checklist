package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Crm;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CrmParcialMapper implements RowMapper<Crm> {
    @Override
    public Crm mapRow(ResultSet rs, int rowNum) throws SQLException {
        Crm crm = new Crm();
        crm.setNumero(rs.getLong("NUM_CONS_PROF_SOLIC"));
        crm.setNumeroConselho(rs.getLong("numero_conselho_profissional"));
        return crm;
    }
}
