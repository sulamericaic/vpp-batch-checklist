package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.impl;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.internacao.SolicitacaoInternacao;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.DetalheInternacaoRepository;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper.DetalheInternacaoRowMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.Optional;

@Repository
public class DetalheInternacaoRepositoryImpl implements DetalheInternacaoRepository {

    private NamedParameterJdbcTemplate provppNamedParameterJdbcTemplate;

    @Inject
    public DetalheInternacaoRepositoryImpl(@Qualifier("provppNamedJdbcTemplate")NamedParameterJdbcTemplate provppNamedParameterJdbcTemplate) {
        this.provppNamedParameterJdbcTemplate = provppNamedParameterJdbcTemplate;
    }

    @Override
    public Optional<SolicitacaoInternacao> buscarDadosInternacao(Long numSolicitacao) {
        StringBuilder sql = new StringBuilder();

        sql.append("    select                                                 ");
        sql.append("    atend.cod_carater_atend_ans,                           ");
        sql.append("            regime.cod_reg_internacao,                     ");
        sql.append("            tipo.cod_tipo_intern                           ");
        sql.append("    from                                                   ");
        sql.append("    ow_provpp.solic_internacao intern                      ");
        sql.append("    join ow_tiss_provpp.tiss_reg_internacao regime on      ");
        sql.append("    intern.num_reg_internacao = regime.num_reg_internacao  ");
        sql.append("    join ow_tiss_provpp.tiss_carater_atend atend on        ");
        sql.append("    intern.num_carater_atend = atend.num_carater_atend     ");
        sql.append("    join ow_tiss_provpp.tiss_tipo_internacao tipo on       ");
        sql.append("    intern.num_tipo_intern = tipo.num_tipo_intern          ");
        sql.append("    where                                                  ");
        sql.append("       num_solicitacao_vpp = :numSolicitacaoVpp            ");

        return this.provppNamedParameterJdbcTemplate.query(sql.toString(),
                new MapSqlParameterSource()
                        .addValue("numSolicitacaoVpp", numSolicitacao),
                new DetalheInternacaoRowMapper()).stream().findFirst();


    }
}
