package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.conselhoprofissional.ConselhoProfissional;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ConselhoProfissionalRowMapper implements RowMapper<ConselhoProfissional> {
    @Override
    public ConselhoProfissional mapRow(ResultSet rs, int rowNum) throws SQLException {
        ConselhoProfissional conselhoProfissional = new ConselhoProfissional();
        conselhoProfissional.setSigla(rs.getString("SIG_CONS_PROFESSIONAL"));
        return conselhoProfissional;
    }
}
