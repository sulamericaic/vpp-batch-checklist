package br.com.sulamerica.susis.vppbatchchecklist.domain.repository;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.beneficiario.Beneficiario;

import java.util.Optional;

public interface BeneficiarioRepository {

    Optional<Beneficiario> buscarDataNascimentoBeneficiario(String codigoEmpresa, String codigoPrefixoEmpresa, String codigoFamiliarBeneficiario, String codigoRdp);

}
