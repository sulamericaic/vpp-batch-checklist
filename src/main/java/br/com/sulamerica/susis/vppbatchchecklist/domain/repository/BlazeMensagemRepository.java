package br.com.sulamerica.susis.vppbatchchecklist.domain.repository;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.blaze.VppBlazeMensagem;

public interface BlazeMensagemRepository {

    void incluiMensagem(Long numeroSolicitacao, VppBlazeMensagem vppBlazeMensagem, int indice);

}
