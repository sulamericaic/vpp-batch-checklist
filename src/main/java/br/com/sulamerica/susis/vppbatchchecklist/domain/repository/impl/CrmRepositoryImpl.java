package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.impl;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.conselhoprofissional.ConselhoProfissional;
import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Crm;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.CrmRepository;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper.ConselhoProfissionalRowMapper;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper.CrmParcialMapper;
import br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper.CrmRowMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class CrmRepositoryImpl implements CrmRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final NamedParameterJdbcTemplate provppNamedParameterJdbcTemplate;

    public CrmRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate,
                             @Qualifier("provppNamedJdbcTemplate")NamedParameterJdbcTemplate provppNamedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.provppNamedParameterJdbcTemplate = provppNamedParameterJdbcTemplate;
    }

    @Override
    public Optional<Crm> buscarConselhoProfissional(Long numeroSolicitacao) {
        StringBuilder sql = new StringBuilder();
        sql.append(" select coalesce(sadt.num_cons_prof_solic, inte.num_cons_prof_solic)     as num_cons_prof_solic,           ");
        sql.append("        coalesce(sadt.num_cons_profissional, inte.num_cons_profissional) as numero_conselho_profissional   ");
        sql.append("        from ow_provpp.solicitacao_vpp vpp                                                                 ");
        sql.append("   full join ow_provpp.solic_sp_sadt    sadt on sadt.num_solicitacao_vpp = vpp.num_solicitacao_vpp         ");
        sql.append("   full join ow_provpp.solic_internacao inte on inte.num_solicitacao_vpp = vpp.num_solicitacao_vpp         ");
        sql.append("   where vpp.num_solicitacao_vpp = :numSolicitacaoVpp                                                      ");

        return this.provppNamedParameterJdbcTemplate.query(sql.toString(),
                new MapSqlParameterSource("numSolicitacaoVpp", numeroSolicitacao), new CrmParcialMapper()).stream()
                .findFirst();
    }

    @Override
    public Optional<Crm> buscarConselhoCompleto(Long numeroConselho, Long codigoUfAns) {
        StringBuilder sql = new StringBuilder();

        sql.append(" SELECT NUM_CRM,                                               ");
        sql.append(" 	   SIG_UF,                                                 ");
        sql.append(" 	   DSC_SIT_CRM                                             ");
        sql.append("   FROM SDE_CADASTRO_CRM CRM                                   ");
        sql.append("  WHERE CRM.NUM_CRM = :numero                                  ");
        sql.append("    AND CRM.SIG_UF = (SELECT ANS.SIG_UF                        ");
        sql.append("                        FROM OW_TISS.UF_ANS ANS                ");
        sql.append("                       WHERE ANS.COD_UF_ANS = :ufSolicitacao   ");
        sql.append("                         AND COD_VERSAO_TISS = 7)              ");

        return this.namedParameterJdbcTemplate.query(sql.toString(), new MapSqlParameterSource()
                .addValue("numero", numeroConselho)
                .addValue("ufSolicitacao", codigoUfAns)
                , new CrmRowMapper())
                .stream().findFirst();
    }

    @Override
    public Optional<ConselhoProfissional> buscaSiglaConselhoProfissional(Long numConselhoProfissional, Long codigoAns) {
        StringBuilder sql = new StringBuilder();
        sql.append("  SELECT SIG_CONS_PROFESSIONAL                                 ");
        sql.append("    FROM OW_TISS.TISS_CONS_PROFISSIONAL                        ");
        sql.append("   WHERE (NUM_CONS_PROFISSIONAL = :codigoConselhoProfissional  ");
        sql.append("      OR  COD_CONS_PROF_ANS     = :codigoConselhoProfissional) ");
        return this.namedParameterJdbcTemplate.query(sql.toString(), new MapSqlParameterSource()
                .addValue("codigoConselhoProfissional", numConselhoProfissional), new ConselhoProfissionalRowMapper())
                .stream().findFirst();
    }

    public Map<String, Object> findDadosConselho(Long numeroSolicitacao){
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT                                                   ");
        sql.append("  SPS.NUM_CONS_PROFISSIONAL,                              ");
        sql.append("  SPS.NUM_CONS_PROF_SOLIC,                                ");
        sql.append("  UFA.SIG_UF,                                             ");
        sql.append("  CONS.SIG_CONS_PROFESSIONAL                              ");
        sql.append(" FROM  OW_PROVPP.SOLIC_SP_SADT SPS                        ");
        sql.append("  JOIN OW_TISS_PROVPP.UF_ANS UFA ON                       ");
        sql.append("   UFA.COD_UF = SPS.COD_UF                                ");
        sql.append("  JOIN OW_TISS_PROVPP.TISS_CONS_PROFISSIONAL CONS ON      ");
        sql.append("   CONS.NUM_CONS_PROFISSIONAL = SPS.NUM_CONS_PROFISSIONAL ");
        sql.append(" WHERE SPS.NUM_SOLICITACAO_VPP = :numeroSolicitacao       ");

        List<Map<String ,Object>> results = provppNamedParameterJdbcTemplate.queryForList(sql.toString(), new MapSqlParameterSource()
                .addValue("numeroSolicitacao", numeroSolicitacao));

        return results.get(0);
    }


}
