package br.com.sulamerica.susis.vppbatchchecklist.domain.repository.mapper;

import br.com.sulamerica.susis.vppbatchchecklist.domain.model.solicitacao.Servico;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ServicoMapper implements RowMapper<Servico> {
    @Override
    public Servico mapRow(ResultSet rs, int rowNum) throws SQLException {
        Servico servico = new Servico();
        servico.setCodigoServico(rs.getInt("COD_SERVICO"));
        return servico;
    }
}
